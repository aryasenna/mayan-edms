# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Stefaniu Criste <gupi@hangar.ro>, 2023
# Harald Ersch, 2023
# Roberto Rosario, 2023
# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-16 23:30+0000\n"
"PO-Revision-Date: 2023-01-05 02:56+0000\n"
"Last-Translator: Roberto Rosario, 2023\n"
"Language-Team: Romanian (Romania) (https://www.transifex.com/rosarior/teams/13584/ro_RO/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ro_RO\n"
"Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));\n"

#: apps.py:28 links.py:10 permissions.py:6
msgid "Task manager"
msgstr "Gestionar de sarcini"

#: apps.py:99
msgid "Label"
msgstr "Conținut etichetă"

#: apps.py:103
msgid "Name"
msgstr "Nume"

#: apps.py:108
msgid "Default queue?"
msgstr "Coadă implicită?"

#: apps.py:113
msgid "Is transient?"
msgstr "Este tranzitorie?"

#: apps.py:117
msgid "Type"
msgstr "Tip"

#: apps.py:122
msgid "Start time"
msgstr "Timpul de începere"

#: apps.py:126
msgid "Host"
msgstr "Gazdă"

#: apps.py:130
msgid "Arguments"
msgstr "Argumente"

#: apps.py:134
msgid "Keyword arguments"
msgstr "Argumentele cuvinte cheie"

#: apps.py:138
msgid "Worker process ID"
msgstr "ID-ul procesului de lucru"

#: permissions.py:10
msgid "View tasks"
msgstr "Vedeți sarcinile"

#: settings.py:11
msgid "Celery"
msgstr "Celery"

#: settings.py:17
msgid "Default: \"AMQPLAIN\". Set custom amqp login method."
msgstr "Implicit: „AMQPLAIN”. Setați metoda de conectare amqp personalizată."

#: settings.py:23
msgid ""
"Default: \"amqp://\". Default broker URL. This must be a URL in the form of:"
" transport://userid:password@hostname:port/virtual_host Only the scheme part"
" (transport://) is required, the rest is optional, and defaults to the "
"specific transports default values."
msgstr ""
"Implicit: \"amqp: //\". Adresa URL a brokerului implicit. Aceasta trebuie să"
" fie o adresă URL sub forma: "
"transport://userid:password@hostname:port/virtual_host Este necesar doar "
"partea sistemului (transport: //), restul este opțional și are implicit "
"valorile implicite pentru transport."

#: settings.py:32
msgid ""
"Default: \"Disabled\". Toggles SSL usage on broker connection and SSL "
"settings. The valid values for this option vary by transport."
msgstr ""
"Implicit: „Dezactivat”. Comută utilizarea SSL la conexiunea brokerului și "
"setările SSL. Valorile valide pentru această opțiune variază în funcție de "
"transport."

#: settings.py:40
msgid ""
"Default: No result backend enabled by default. The backend used to store "
"task results (tombstones). Refer to "
"http://docs.celeryproject.org/en/v4.1.0/userguide/configuration.html#result-"
"backend"
msgstr ""
"Implicit: niciun rezultat de backend activat în mod implicit. Backend-ul "
"folosit pentru a stoca rezultatele sarcinilor (pietre funerare). Consultați "
"http://docs.celeryproject.org/en/v4.1.0/userguide/configuration.html#result-"
"backend "

#: tests/literals.py:11
msgid "Test queue"
msgstr "Coadă de așteptare de test"

#: views.py:13
msgid "Background task queues"
msgstr "Cozi de sarcini în fundal"
